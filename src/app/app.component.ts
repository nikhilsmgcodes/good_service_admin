import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';
// import * as $ from 'jquery';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent{
  public hideSideBar : boolean = false;
  constructor(public router: Router, public location: Location){
    router.events.subscribe((val) => {
      console.log(location.path());
      if(location.path()=='/login'){
        this.hideSideBar = true;
        if(localStorage.getItem('loginData')){
          // this.router.navigate(['/dashboard']);
        }
      }else{
        if(!localStorage.getItem('loginData')){
          // this.router.navigate(['/login']);
        }
        this.hideSideBar = false;
      }
      // if(location.path() != ''){
      //   this.route = location.path();
      // } else {
      //   this.route = 'Home'
      // }
    });
  }
  ngOnInit(){
    // console.log(this.router.url);
  }
}
