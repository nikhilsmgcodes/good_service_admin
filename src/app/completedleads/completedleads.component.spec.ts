import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompletedleadsComponent } from './completedleads.component';

describe('CompletedleadsComponent', () => {
  let component: CompletedleadsComponent;
  let fixture: ComponentFixture<CompletedleadsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompletedleadsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompletedleadsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
